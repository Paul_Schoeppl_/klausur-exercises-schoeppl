package service;

import java.util.*;

public class BuildPlanner {

    private final SortedMap<Character, Set<Character>> requirements;

    private static final String VALID_PATTERN = "Step [A-Z] must be finished before step [A-Z] can begin.";

    public BuildPlanner(String... input) {
        requirements = new TreeMap<>();

        for (String line : input) {
            if (line.matches(VALID_PATTERN)) {
                addStep(line);
            }
        }
    }

    private void addStep(String requirement) {
        String[] parts = requirement.split("\\s");
        Character preStep = parts[1].charAt(0);
        Character postStep = parts[7].charAt(0);

        requirements.putIfAbsent(postStep, new HashSet<>());
        requirements.get(postStep).add(preStep);

        requirements.putIfAbsent(preStep, new HashSet<>());
    }

    public SortedMap<Character, Set<Character>> getRequirements() {
        return new TreeMap<>(requirements);
    }

    public Queue<Character> order() throws OrderException {
        Queue<Character> resultQueue = new ArrayDeque<>();

        while (resultQueue.size() < requirements.size()) {
            int originalSize = resultQueue.size();
            for (var entry : requirements.entrySet()) {
                if (resultQueue.containsAll(entry.getValue()) && !resultQueue.contains(entry.getKey())) {
                    resultQueue.offer(entry.getKey());
                    break;
                }
            }
            if (originalSize == resultQueue.size()) throw new IllegalArgumentException("something wrong");
        }

        return resultQueue;
    }
}