package service;

import java.util.ArrayList;
import java.util.List;

public class Publisher {
    private final List<Subscriber> stuff = new ArrayList<>();

    public void addOther(Subscriber other) {
        stuff.add(other);
    }

    public void removeOther(Subscriber other) {
        stuff.remove(other);
    }

    private void notifyOther() {
        for (var other : stuff) {
            other.call("context");
        }
    }
}
