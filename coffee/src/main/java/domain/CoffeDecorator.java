package domain;

public class CoffeDecorator implements ICoffee {

    ICoffee wrappee;

    public CoffeDecorator(ICoffee wrappee) {
        this.wrappee = wrappee;
    }

    @Override
    public long getCost() {
        return wrappee.getCost();
    }

    @Override
    public String getZutaten() {
        return wrappee.getZutaten();
    }
}
