package domain;

public interface ICoffee {
    long getCost();
    String getZutaten();
}
