package domain;

import java.util.StringJoiner;

public class CoffeeWithMilk extends CoffeDecorator {

    private final MilchArten milchArt;

    private final long zusatzPreis;

    public CoffeeWithMilk(MilchArten milchArt, long zusatzPreis, ICoffee wrappee) {
        super(wrappee);
        this.milchArt = milchArt;
        this.zusatzPreis = zusatzPreis;
    }

    @Override
    public long getCost() {
        return super.getCost() + zusatzPreis;
    }

    @Override
    public String getZutaten() {
        return new StringJoiner(", ")
                .add(super.getZutaten())
                .add(milchArt.toString())
                .toString();
    }
}
