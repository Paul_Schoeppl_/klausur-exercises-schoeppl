package domain;


public record Coffee(long cost) implements ICoffee{
    @Override
    public long getCost() {
        return cost;
    }

    public String getZutaten(){
        return "Coffee";
    }
}
