package domain;

import java.util.StringJoiner;

public class CoffeeWithSirup extends CoffeDecorator {
    private final SirupArten sirupArt;
    private final long zusatzPreis;

    public CoffeeWithSirup(SirupArten sirupArt, long zusatzPreis, ICoffee wrappee) {
        super(wrappee);
        this.sirupArt = sirupArt;
        this.zusatzPreis = zusatzPreis;
    }

    @Override
    public long getCost() {
        return super.getCost() + zusatzPreis;
    }

    @Override
    public String getZutaten() {
        return new StringJoiner(", ")
                .add(super.getZutaten())
                .add(sirupArt.toString())
                .toString();
    }


}
