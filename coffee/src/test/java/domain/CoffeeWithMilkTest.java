package domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CoffeeWithMilkTest {

    @Test
    void gesamtKostenKorrekt() {
        var coffee = new CoffeeWithMilk(MilchArten.Soja, 3, new Coffee(3));

        long cost = coffee.getCost();

        assertEquals(6, cost);

    }

    @Test
    void getZutaten() {
        var coffee = new CoffeeWithMilk(MilchArten.Soja, 3, new Coffee(3));

        String zutaten = coffee.getZutaten();

        assertEquals("Coffee, Soja", zutaten);
    }

    @Test
    void priceWithComplexCombinationsCorrect() {
        var coffee = new CoffeeWithMilk(MilchArten.Hafer, 3,
                new CoffeeWithSirup(SirupArten.Karamel, 5,
                        new Coffee(2)));

        long price = coffee.getCost();

        assertEquals(10, price);
    }
    @Test
    void nameWithComplexCombinationsCorrect() {
        var coffee = new CoffeeWithMilk(MilchArten.Hafer, 3,
                new CoffeeWithSirup(SirupArten.Karamel, 5,
                        new Coffee(2)));

        String zutaten = coffee.getZutaten();

        assertEquals("Coffee, Karamel, Hafer", zutaten);

    }




}