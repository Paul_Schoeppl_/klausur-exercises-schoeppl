package service;

public interface IDecoder {
    Object decode(String json);
}
