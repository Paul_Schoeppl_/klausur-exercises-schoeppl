package service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Stream;

import org.springframework.util.ResourceUtils;


public class DecodeProxy implements IDecoder {

    private final IDecoder service;

    private final List<String> forbiddenTerms;

    public DecodeProxy(IDecoder service, String pathToForbiddenTerms) {
        this.service = service;
        try (Stream<String> forbiddenWords = Files.lines(ResourceUtils.getFile("classpath:" + pathToForbiddenTerms).toPath())) {
            this.forbiddenTerms = forbiddenWords.toList();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public Object decode(String json) {
        for (var word : forbiddenTerms) {
            if (json.contains(word)) throw new IllegalArgumentException("input text is not brandsafe");
        }
        return service.decode(json);
    }
}
