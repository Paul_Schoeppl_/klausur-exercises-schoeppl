package app;

import service.DecodeProxy;
import service.Decoder;

import java.io.IOException;
import java.net.URISyntaxException;

public class Application {

    public static void main(String[] args) throws URISyntaxException, IOException {
        var json = """
                   {
                       "title": "Complex Analysis",
                       "author": "Tristan Needham"
                   }
                """;
        var decoder = new Decoder();
        var decoderWithCensor = new DecodeProxy(decoder, "forbidden.txt");
        try{
            var decoded = decoderWithCensor.decode(json);
        }catch(IllegalArgumentException e){
            System.out.println("Censor worked successfully");
        }
    }
}
