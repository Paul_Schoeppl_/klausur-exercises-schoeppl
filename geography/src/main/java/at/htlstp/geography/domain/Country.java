package at.htlstp.geography.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Pattern;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.Hibernate;

import java.util.Objects;

@Getter
@Setter
@Entity
@Table(name = "countries")
@AllArgsConstructor
@NoArgsConstructor
public class Country {
    @Id
    @Column(name = "code", nullable = false)
    @Pattern(regexp = "[A-Z][A-Z]")
    private String code;

    @Pattern(regexp = "[A-Z].*")
    @Column(name = "country_name")
    private String name;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Country country = (Country) o;
        return getCode() != null && Objects.equals(getCode(), country.getCode());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}