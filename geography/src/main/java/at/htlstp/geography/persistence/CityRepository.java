package at.htlstp.geography.persistence;

import at.htlstp.geography.domain.City;
import at.htlstp.geography.domain.Country;
import jakarta.validation.constraints.Positive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Integer> {
    @Query("select c from City c where c.country = ?1")
    List<City> findAllByCountry(Country country);

    List<City> findAllByPopulationBetweenAndCountry(@Positive Long population, @Positive Long population2, Country country);

    List<City> findAllByPopulationBetween(@Positive Long population, @Positive Long population2);
}