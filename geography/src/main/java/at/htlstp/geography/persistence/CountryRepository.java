package at.htlstp.geography.persistence;

import at.htlstp.geography.domain.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, String> {
}