package at.htlstp.geography.presentation.api;

import at.htlstp.geography.domain.City;
import at.htlstp.geography.domain.Country;
import at.htlstp.geography.persistence.CityRepository;
import at.htlstp.geography.persistence.CountryRepository;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@org.springframework.web.bind.annotation.RestController
public class RestController {
    private final CountryRepository countryRepository;
    private final CityRepository cityRepository;

    public RestController(CountryRepository countryRepository,
                          CityRepository cityRepository) {
        this.countryRepository = countryRepository;
        this.cityRepository = cityRepository;
    }

    @GetMapping("/countries/{code}")
    public Country getCountryByCode(@PathVariable(name = "code") String code) {
        return countryRepository.findById(code).orElseThrow();
    }

    @PostMapping("/countries")
    public ResponseEntity<Country> postCountry(@Valid @RequestBody Country country) {
        if (countryRepository.existsById(country.getCode())) {
            throw new IllegalArgumentException("country already exists");
        }
        Country saved = countryRepository.save(country);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(saved.getCode());
        return ResponseEntity
                .created(uri)
                .body(saved);
    }

    @GetMapping("/cities")
    public List<City> getCities(@RequestParam(name = "max", required = false) Long max,
                                @RequestParam(name = "min", required = false) Long min,
                                @RequestParam(name = "country", required = false) String country) {
        if (min == null) min = 1L;
        if (max == null) max = Long.MAX_VALUE;
        if (country == null) return cityRepository.findAllByPopulationBetween(min, max);
        return cityRepository.findAllByPopulationBetweenAndCountry(min, max, countryRepository.findById(country).orElseThrow());
    }


}
