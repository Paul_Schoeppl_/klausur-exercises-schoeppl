package at.htlstp.geography.presentation.web;

import at.htlstp.geography.domain.City;
import at.htlstp.geography.persistence.CityRepository;
import at.htlstp.geography.persistence.CountryRepository;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WebController {
    private final CityRepository cityRepository;
    private final CountryRepository countryRepository;

    public WebController(CityRepository cityRepository,
                         CountryRepository countryRepository) {
        this.cityRepository = cityRepository;
        this.countryRepository = countryRepository;
    }

    @GetMapping("/overview")
    public String getOverview(@RequestParam(name="selected_country", required = false) String country, Model model){
        if(country != null){
            model.addAttribute("all_cities", cityRepository.findAllByCountry(countryRepository.findById(country).orElseThrow()));
        }
        model.addAttribute("all_countries", countryRepository.findAll());
        return "overview";
    }

    @GetMapping("/overview/new")
    public String getNewCityPage(Model model){
        if(!model.containsAttribute("new_city")){
            model.addAttribute("new_city", new City());
        }
        model.addAttribute("all_countries", countryRepository.findAll());
        return "new_city";
    }

    @PostMapping("/overview/new")
    public String postNewCity(@Valid @ModelAttribute(name="new_city") City city, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors())
            return getNewCityPage(model);
        cityRepository.save(city);
        return "redirect:/overview?selected_country=" + city.getCountry().getCode();
    }
}
