package service;

import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

public class OrbitService {
    private SortedMap<String, String> orbitMap = new TreeMap<>();

    public OrbitService(String... orbits) {
        for (String orbit : orbits) {
            addOrbit(orbit);
        }
        getRoot();
    }

    private void addOrbit(String orbit) {
        String[] parts = orbit.split("\\)");
        if (parts.length != 2 || orbit.startsWith(")") || orbit.endsWith(")"))
            throw new IllegalArgumentException();
        String root = parts[0];
        String orbitee = parts[1];

        orbitMap.putIfAbsent(orbitee, root);
    }


    public String getRoot() {
        Set<String> orbitees = orbitMap.keySet();
        Set<String> roots = new HashSet<>(orbitMap.values());
        roots.removeAll(orbitees);
        if (roots.size() != 1) throw new IllegalArgumentException();
        for (var entry : roots) {
            return entry;
        }
        throw new IllegalArgumentException();
    }

    public int totalDistance() {
        int longestDistance = 0;
        for (String planet : orbitMap.keySet()) {
            longestDistance += totalDistance(0, planet);
        }
        return longestDistance;
    }

    public int totalDistance(int distance, String planet) {
        if (!orbitMap.containsKey(planet)) return distance;
        return totalDistance(++distance, orbitMap.get(planet));
    }
}
