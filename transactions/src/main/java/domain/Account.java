package domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

@AllArgsConstructor
public class Account {
    private Set<Subscriber> subscriberList = new HashSet<>();

    @Getter
    @NonNull
    private final String iban;
    @Getter
    private BigDecimal balance;
    @Getter
    private BigDecimal limit;

    public Account(@NonNull String iban, BigDecimal balance, BigDecimal limit) {
        this.iban = iban;
        this.balance = balance;
        this.limit = limit;
    }

    /**
     * Increases the balance by a given amount.
     *
     * @param amount amount to be deposited
     * @throws IllegalArgumentException if amount is < 0
     */
    public void deposit(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.valueOf(0)) < 0) {
            notifySubscribers("Transaction with amount " + amount + "at " + Instant.now() + " not successful");
            throw new IllegalArgumentException();
        }
        balance = balance.add(amount);
        notifySubscribers("Transaction with amount " + amount + "at " + Instant.now() + " successful");
    }

    public void addSubsriber(Subscriber subscriber) {
        subscriberList.add(subscriber);
    }

    public void removeSubsriber(Subscriber subscriber) {
        subscriberList.remove(subscriber);
    }

    /**
     * Decreases the balance by a given amount. The amount must be 0 <= amount <= limit.
     *
     * @param amount amount to be withdrawn
     * @throws IllegalArgumentException if amount < 0 or amount > limit
     */
    public void withdraw(BigDecimal amount) {
        if (amount.compareTo(BigDecimal.valueOf(0)) < 0 || amount.compareTo(limit) > 0) {
            notifySubscribers("Transaction with amount " + amount + "at " + Instant.now() + "not successful");
            throw new IllegalArgumentException();
        }
        balance = balance.subtract(amount);
        notifySubscribers("Transaction with amount " + amount + "at " + Instant.now() + "successful");
    }

    /**
     * Sets a new limit.
     *
     * @param limit the desired limit
     * @throws IllegalArgumentException if limit is < 0
     */
    public void setLimit(BigDecimal limit) {
        if (limit.compareTo(BigDecimal.valueOf(0)) < 0) {
            notifySubscribers("Transaction with amount " + limit + "at " + Instant.now() + "not successful");
            throw new IllegalArgumentException();
        }
        this.limit = limit;
        notifySubscribers("Transaction with amount " + limit + "at " + Instant.now() + "successful");
    }

    public void notifySubscribers(String message) {
        subscriberList.forEach(s -> s.update(message));
    }
}
