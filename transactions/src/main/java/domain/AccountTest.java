package domain;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    @Test
    void depositWorks() {
        var account = new Account("123", BigDecimal.valueOf(3), BigDecimal.valueOf(300));

        account.deposit(BigDecimal.valueOf(30));

        assertEquals(BigDecimal.valueOf(33), account.getBalance());
    }

    @Test
    void negativeDepositThrowsException() {
        var account = new Account("123", BigDecimal.valueOf(3), BigDecimal.valueOf(300));

        assertThrows(IllegalArgumentException.class, () -> {
            account.deposit(BigDecimal.valueOf(-30));
        });
    }

    @Test
    void subtractionWorks(){
        var account = new Account("123", BigDecimal.valueOf(3), BigDecimal.valueOf(300));

        account.withdraw(BigDecimal.valueOf(2));

        assertEquals(BigDecimal.valueOf(1), account.getBalance());
    }

    @Test
    void negativeSubtractionFails(){
        var account = new Account("123", BigDecimal.valueOf(3), BigDecimal.valueOf(300));

        assertThrows(IllegalArgumentException.class, ()-> account.withdraw(BigDecimal.valueOf(-1)));
    }

    @Test
    void SubtractionBeyondLimitFails(){
        var account = new Account("123", BigDecimal.valueOf(3), BigDecimal.valueOf(300));

        assertThrows(IllegalArgumentException.class, ()-> account.withdraw(BigDecimal.valueOf(301)));
    }
}