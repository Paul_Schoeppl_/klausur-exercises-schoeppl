package domain;

public class ConsoleListener implements Subscriber{
    @Override
    public void update(String message) {
        System.out.println(message);
    }
}
