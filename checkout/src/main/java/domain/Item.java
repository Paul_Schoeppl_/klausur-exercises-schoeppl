package domain;

import java.util.Objects;

/**
 * Ein konkreter Artikel im Einkaufskorb
 */
public record Item(Product product, int quantity) {

    public Item {
        if (quantity <= 0 || product == null) throw new IllegalArgumentException();
    }

    public Item(Product product) {
        this(product, 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return quantity == item.quantity && Objects.equals(product, item.product);
    }

    @Override
    public int hashCode() {
        return Objects.hash(product, quantity);
    }
}
