package domain;

import lombok.NonNull;

import java.util.Map;
import java.util.Objects;
import java.util.TreeMap;

/**
 * Ein Angebot für einen bestimmten Artikel zu einem bestimmten Preis
 */
public record Offer(int quantity, @NonNull Price price) {

    public Offer {
        if (quantity <= 0) throw new IllegalArgumentException("Quantity < 0");
    }

    public double getSinglePrice() {
        return price.getGesamtCent() / (double) quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Offer offer = (Offer) o;
        return quantity == offer.quantity && price.equals(offer.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(quantity, price);
    }
}
