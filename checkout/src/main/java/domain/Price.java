package domain;

import java.util.Collection;
import java.util.Objects;

/**
 * Ein Preis in Euro und Cent
 */
public record Price(int euro, int cent) {

    public Price {
        if (euro < 0 || cent < 0) throw new IllegalArgumentException();

        long cents = euro * 100L + cent;
        euro = (int) cents / 100;
        cent = (int) (cents % 100);
    }

    /**
     * Sums all prices.
     *
     * @param prices the prices
     * @return the sum
     */
    public static Price sum(Collection<Price> prices) {
        if (prices == null || prices.isEmpty()) return new Price(0, 0);
        return prices.stream().reduce((Price::add)).orElseThrow();
    }

    /**
     * Adds a given price to this, returning a new Price.
     *
     * @param price price to add
     * @return new Price containing sum of this and price
     */
    public Price add(Price price) {
        long centSum = (this.euro + price.euro) * 100L + this.cent + price.cent;
        return new Price(0, (int) centSum);
    }

    /**
     * Multiplies this by n, returning a new Price.
     *
     * @param n the factor
     * @return new Price containing the product of this and n
     */
    public Price multiply(int n) {
        int centSumMultiplied = n * (100 * this.euro + this.cent);
        return new Price(0, centSumMultiplied);
    }

    public int getGesamtCent(){
        return 100 * euro + cent;
    }

    @Override
    public String toString() {
        return euro + "." + String.format("%02d", cent);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return euro == price.euro && cent == price.cent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(euro, cent);
    }
}
