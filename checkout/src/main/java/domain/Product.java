package domain;

import lombok.EqualsAndHashCode;

import java.util.*;
/**
 * Ein Produkt inklusive Angeboten
 */
public record Product(String description, Set<Offer> offers) {

    public Product {
        if (description == null || description.isBlank()) throw new IllegalArgumentException("Description ist leer");
        if (offers == null || offers.isEmpty()) throw new IllegalArgumentException("Offers sind empty");
        if (offers.stream().map(Offer::quantity).noneMatch(q -> q == 1))
            throw new IllegalArgumentException("offers haben keinen Einzelpreis");
    }

    public Product(String description, Price price) {
        this(description, Set.of(new Offer(1, price)));
    }

    public Price getCheapestPriceForQuantity(int requiredQuantity) {
        SortedMap<Double, Offer> map = new TreeMap<>();
        for (var offer : offers) {
            map.put(offer.getSinglePrice(), offer);
        }
        Price gesamtPreis = new Price(0, 0);
        for (var entry : map.values()) {
            while (entry.quantity() <= requiredQuantity) {
                gesamtPreis = gesamtPreis.add(entry.price());
                requiredQuantity -= entry.quantity();
            }
        }
        return gesamtPreis;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return Objects.equals(description, product.description) && Objects.equals(offers, product.offers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(description, offers);
    }
}
