package domain;


import javafx.scene.paint.Color;

public class CircularToCircleAdapter implements Shape{
    public CircularToCircleAdapter(Circular adaptee) {
        this.adaptee = adaptee;
    }

    private final Circular adaptee;
    public void setColor(Color color) {
        int red = (int) (color.getRed() * 255);
        int green = (int) (color.getRed() * 255);
        int blue = (int) (color.getRed() * 255);
        adaptee.setColor(new java.awt.Color(red, green, blue));
    }

    public void scale(float factor) {
        adaptee.scale(factor);
    }
}
