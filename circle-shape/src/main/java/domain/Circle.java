package domain;

import javafx.scene.paint.Color;

public class Circle implements Shape {
    private Circular circular;

    public Circle(Circular circular) {
        this.circular = circular;
    }

    @Override
    public void setColor(Color color) {
        int red = (int) (color.getRed() * 255);
        int green = (int) (color.getGreen() * 255);
        int blue = (int) (color.getBlue() * 255);
        int alpha = (int) (color.getOpacity() * 255);
        circular.setColor(new java.awt.Color(red, green, blue, alpha));
    }

    @Override
    public void scale(float factor) {
        circular.scale(factor);
    }
}
