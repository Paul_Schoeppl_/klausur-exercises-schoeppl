package at.htlstp.spacehistory.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import lombok.*;
import org.hibernate.Hibernate;

import java.time.LocalDate;
import java.util.Objects;

/**
 * Diese Klasse existiert in der aktuellen Form NUR, damit CsvParser kompiliert.
 * Wahrscheinlich möchten Sie den kompletten Code löschen
 */

@Entity
@Table(name = "launches")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Launch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String company;

    @PastOrPresent(message = "must be in the past or present")
    @NotNull(message = "must not be null")
    private LocalDate date;

    private String rocket;

    private boolean success;

    public Launch(String company, LocalDate date, String rocket, boolean success) {
        this.company = company;
        this.date = date;
        this.rocket = rocket;
        this.success = success;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Launch launch = (Launch) o;
        return getId() != null && Objects.equals(getId(), launch.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }

    public String getSuccessAsString() {
        return success ? "success" : "failure";
    }
}
