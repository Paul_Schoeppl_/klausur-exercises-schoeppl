package at.htlstp.spacehistory.presentation;

import at.htlstp.spacehistory.domain.Launch;
import at.htlstp.spacehistory.persistence.LaunchRepository;
import jakarta.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public record SpaceController(LaunchRepository launchRepository) {

    @GetMapping("overview")
    public String showOverview(Model model) {
        model.addAttribute("all_launches", launchRepository.findAllInDescOrder());
        return "overview";
    }

    @GetMapping("/companies/{name}")
    public String showOverviewForCompany(Model model, @PathVariable(name = "name") String companyName) {
        model.addAttribute("company_name", companyName);
        model.addAttribute("launches_by_company", launchRepository.findAllByCompany(companyName));
        return "company";
    }

    @GetMapping("/new")
    public String showNewLaunchPage(Model model) {
        if (!model.containsAttribute("new_launch")) {
            model.addAttribute("new_launch", new Launch());
            model.addAttribute("all_companies", launchRepository.findAllCompanies());
        }
        return "new_launch";
    }

    @PostMapping("/new")
    public String postNewLaunch(@Valid @ModelAttribute("new_launch") Launch launch, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return showNewLaunchPage(model);
        }
        launchRepository.save(launch);
        return "redirect:/overview";
    }
}
