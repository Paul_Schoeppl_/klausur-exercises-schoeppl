package at.htlstp.spacehistory.setup;

import at.htlstp.spacehistory.persistence.LaunchRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Dient dazu, Startdaten in die Datenbank zu speichern.
 * Initial werden nur 4 Launches eingetragen, es sollten allerdings alle aus space-history.csv geladen werden.
 */
@Component
public record CsvLoader(CsvParser parser, LaunchRepository launchRepository) implements CommandLineRunner {

    @Override
    public void run(String... args) throws Exception {
        var launches = parser.getStartingData();
        launchRepository.saveAll(launches);
    }
}
