package at.htlstp.spacehistory.setup;

import at.htlstp.spacehistory.domain.Launch;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Parst Launches in einem Format ähnlich wie in space-history.csv.
 */
@Component
record CsvParser() {

    /**
     * NICHT ANFASSEN, kann wie deklariert verwendet werden
     * E -> weekday
     * MMM -> month
     * dd -> day
     * uuuu -> year
     * [] -> optional
     * HH -> hour
     * mm -> minute
     * z -> Zeitzone
     * NICHT ANFASSEN, kann wie deklariert verwendet werden
     */
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E MMM dd uuuu[ HH:mm z]", Locale.US);

    /**
     * Dient nur zum Laden einiger Initialdaten, kann gelöscht werden, wenn das Parsing aus der csv-Datei funktioniert.
     *
     * @return DemoDaten
     */
    Collection<Launch> getStartingData() {
//        return List.of(
//                new Launch(
//                        "SpaceX",
//                        LocalDate.parse("Fri Aug 07 2020 05:12 UTC", formatter),
//                        "Falcon 9 Block 5",
//                        true),
//                new Launch(
//                        "CASC",
//                        LocalDate.parse("Thu Aug 06 2020 04:01 UTC", formatter),
//                        "Long March 2D",
//                        true),
//                new Launch(
//                        "ISA",
//                        LocalDate.parse("Sun Feb 09 2020 15:48 UTC", formatter),
//                        "Simorgh",
//                        false),
//                new Launch(
//                        "SpaceX",
//                        LocalDate.parse("Tue Aug 04 2020 23:57 UTC", formatter),
//                        "Starship Prototype",
//                        true)
//        );
        try (Stream<String> lines = Files.lines(ResourceUtils.getFile("classpath:space-history.csv").toPath())) {
            return lines
                    .skip(1)
                    .map(s -> s.split(","))
                    .map(s -> new Launch(s[0], LocalDate.parse(s[2], formatter), s[3].split("\\|")[0], s[6].equalsIgnoreCase("success")))
                    .toList();
        } catch (IOException e) {
            throw new IllegalArgumentException();
        }
    }
}
