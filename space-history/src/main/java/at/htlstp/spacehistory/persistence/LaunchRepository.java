package at.htlstp.spacehistory.persistence;

import at.htlstp.spacehistory.domain.Launch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LaunchRepository extends JpaRepository<Launch, Long> {
    @Query("select l from Launch l order by l.date desc")
    List<Launch> findAllInDescOrder();
    @Query("select l from Launch l where l.company = ?1 order by l.date desc")
    List<Launch> findAllByCompany(String company);

    @Query("""
        select distinct launch.company
        from Launch launch
""")
    List<String> findAllCompanies();
}
