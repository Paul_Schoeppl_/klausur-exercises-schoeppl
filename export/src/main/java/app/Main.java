package app;

import domain.Document;
import service.Exporter;

public class Main {
    public static void main(String[] args) {
        var exporter = new Exporter();
        var document = new Document();
        var pdf = exporter.export(document, "PDF");
        System.out.println(pdf);
    }
}
