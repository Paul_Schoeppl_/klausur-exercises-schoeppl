package service;

import domain.Document;
import domain.PDF;
import domain.XML;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class Exporter {

    public Document export(Document d, String typeOfExport) {
        if (typeOfExport.equals("PDF"))
            return new PDF(d);
        else if (typeOfExport.equals("XML"))
            return new XML(d);
        else throw new IllegalArgumentException("Type not recognized");
    }
}
