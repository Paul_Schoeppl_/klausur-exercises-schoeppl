package service;

import java.util.HashMap;
import java.util.Map;

public class PrimeFinder {

    private static PrimeFinder primeFinderInstance;

    private final Map<Long, Boolean> primesMap;

    private PrimeFinder() {
        primesMap = new HashMap<>();
        for(long i = 2; i <= 10_000; i++){
            isPrime(i);
        }
    }

    boolean isPrime(long candidate) {
        if (primesMap.containsKey(candidate)) {
            System.out.println("already cached");
            return primesMap.get(candidate);
        }

        int root = (int) Math.sqrt(candidate);
        for (int i = 2; i <= root; i++) {
            if ((candidate % i) == 0) {
                primesMap.put(candidate, false);
                return false;
            }
        }
        primesMap.put(candidate, true);
        return true;
    }

    public static PrimeFinder getInstance() {
        if (primeFinderInstance == null)
            primeFinderInstance = new PrimeFinder();
        return primeFinderInstance;
    }

}
