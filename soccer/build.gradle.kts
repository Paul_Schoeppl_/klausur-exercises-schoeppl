plugins {
    id("java")
}

group = "at.htlstp"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}
dependencies {
    implementation("org.projectlombok:lombok:1.18.22")
}


tasks.getByName<Test>("test") {
    useJUnitPlatform()
}