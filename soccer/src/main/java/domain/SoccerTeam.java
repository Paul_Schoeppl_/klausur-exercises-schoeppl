package domain;

import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@NoArgsConstructor
public class SoccerTeam {
    private Formation formation;

    public void play(){
        formation.play();
    }
}
