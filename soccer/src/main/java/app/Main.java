package app;

import domain.Formation433;
import domain.Formation442;
import domain.SoccerTeam;

public class Main {
    public static void main(String []args) {
        SoccerTeam team = new SoccerTeam();
        team.setFormation(new Formation433());
        team.play();
        team.setFormation(new Formation442());
        team.play();
    }
}
