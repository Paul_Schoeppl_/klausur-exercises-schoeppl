package at.htlstp.bookings.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.hibernate.Hibernate;
import org.springframework.boot.context.properties.bind.DefaultValue;

import java.util.Objects;

@Entity
@Table(name="tables")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class TableClass {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="table_id")
    private Long id;
    @Min(1)
    @Column(name="size")
    @NotNull
    private Integer size;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        TableClass table = (TableClass) o;
        return getId() != null && Objects.equals(getId(), table.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
