package at.htlstp.bookings.domain;

import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.web.client.HttpStatusCodeException;

public class ReservationNotFound extends HttpStatusCodeException {
    public ReservationNotFound() {
        super(HttpStatus.NOT_FOUND);
    }
}
