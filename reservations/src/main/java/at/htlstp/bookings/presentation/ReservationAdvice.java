package at.htlstp.bookings.presentation;

import at.htlstp.bookings.domain.ReservationNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ProblemDetail;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ResponseBody
@ControllerAdvice
public class ReservationAdvice {
    @ExceptionHandler(ReservationNotFound.class)
    ProblemDetail handleStudentNotFound(ReservationNotFound ex) {
        return ProblemDetail.forStatus(HttpStatus.NOT_FOUND);
    }
}
