package at.htlstp.bookings.presentation;

import at.htlstp.bookings.domain.Reservation;
import at.htlstp.bookings.domain.ReservationNotFound;
import at.htlstp.bookings.persistence.TableClassRepository;
import at.htlstp.bookings.persistence.ReservationRepository;
import jakarta.validation.Valid;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/reservations")
public record ReservationController(ReservationRepository reservationRepository,
                                    TableClassRepository tableClassRepository) {

    @GetMapping
    public List<Reservation> getReservationsForGuest(@RequestParam(name = "name", required = false) String name) {
 //       if (name == null) return reservationRepository.findAll();
        return reservationRepository.findAllByGuestName(name);
    }

    @GetMapping("/{id}")
    public Reservation getIndividualReservationById(@PathVariable(name = "id") Long id) {
        return reservationRepository.findById(id).orElseThrow(ReservationNotFound::new);
    }

    @PostMapping
    public ResponseEntity<Reservation> postReservation(@Valid @RequestBody Reservation reservation) {
        if (!reservationRepository().getOverlappingReservations(
                reservation.getTable(),
                reservation.getTime().minusHours(1).minusMinutes(59).minusSeconds(59),
                reservation.getTime().plusHours(1).plusMinutes(59).plusSeconds(59)
        ).isEmpty()) {
            System.out.println("Zeiten überlappen sich");
            return new ResponseEntity<>(HttpStatusCode.valueOf(400));
        }
        if (!isGroupSizeValid(reservation)) return new ResponseEntity<>(HttpStatusCode.valueOf(400));
        var saved = reservationRepository.save(reservation);
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .build(saved.getId());
        return ResponseEntity
                .created(uri)
                .body(saved);
    }

    private boolean isGroupSizeValid(Reservation reservation) {
        var table = tableClassRepository.findById(reservation.getTable().getId()).orElseThrow();
        return reservation.getGroupSize() <= table.getSize();
    }
}
