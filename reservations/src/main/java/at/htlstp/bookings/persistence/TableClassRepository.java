package at.htlstp.bookings.persistence;

import at.htlstp.bookings.domain.TableClass;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TableClassRepository extends JpaRepository<TableClass, Long> {
}