package at.htlstp.bookings.persistence;

import at.htlstp.bookings.domain.Reservation;
import at.htlstp.bookings.domain.TableClass;
import org.springframework.cglib.core.Local;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;


public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    @Query("""
            select reservation
            from Reservation reservation
            join reservation.guest guest
            where guest.name = :name
            """)
    List<Reservation> findAllByGuestName(String name);

    @Query("""
        select reservation
        from Reservation reservation
        where reservation.table = :table
        and reservation.time between :startTime and :endTime
""")
    List<Reservation> getOverlappingReservations(TableClass table, LocalDateTime startTime, LocalDateTime endTime);

}