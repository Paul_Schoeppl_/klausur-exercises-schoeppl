package service;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class ServiceTest {
    private final Service service = new Service();

    @ParameterizedTest
    @ValueSource(ints = {1, 5, 7, 11})
    void returnsTrueForRealPrimes(int number) {
        assertTrue(service.isPrime(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {6, 9, 21})
    void returnsFalseForFakePrimes(int number) {
        assertFalse(service.isPrime(number));
    }

    @ParameterizedTest
    @ValueSource(ints = {-1, -3})
    void isPrimeFalseWithNegativeNumbers(int number) {
        assertFalse(service.isPrime(number)); //failed
    }

    @org.junit.jupiter.api.Test
    void min() {
        int[] numbs = new int[]{-1, 3, -4, 7};
        assertEquals(-4, service.min(numbs));
    }

    @org.junit.jupiter.api.Test
    void reverse() {
        assertEquals(511, service.reverse(115));
    }

    @org.junit.jupiter.api.Test
    void isPalindrome() {
        assertTrue(service.isPalindrome("101"));
        assertFalse(service.isPalindrome("1002"));
    }

    @org.junit.jupiter.api.Test
    void areAmicableNumbers() {
    }

    @org.junit.jupiter.api.Test
    void isAnagram() {
    }
}